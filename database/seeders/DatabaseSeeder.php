<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TypeTableSeeder::class);
        $this->call(ExpansionTableSeeder::class);
        $this->call(RarityTableSeeder::class);
        $this->call(PowerTypesTableSeeder::class);
        $this->call(PowersTableSeeder::class);
        $this->call(CardTableSeeder::class);
        $this->call(UserTableSeeder::class);
    }
}
