<?php

namespace Database\Seeders;

use App\Repositories\Expansion\Expansion;
use Illuminate\Database\Seeder;

class ExpansionTableSeeder extends Seeder
{
    private $expansions;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->expansions = collect(json_decode(file_get_contents('database/json/expansions.json'),true))->map(function($expansion) {
            return [
                'name' => $expansion['name'],
            ];
        });

        foreach ($this->expansions as $expansion) {
            $x = new Expansion();
            $x->name = $expansion['name'];
            $x->save();
        }
    }
}
