<?php

namespace Database\Seeders;

use App\Repositories\Rarity\Rarity;
use Illuminate\Database\Seeder;

class RarityTableSeeder extends Seeder
{
    private $rarities;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->rarities = collect(json_decode(file_get_contents('database/json/rarities.json'),true))->map(function($rarity) {
            return [
                'name' => $rarity['name'],
            ];
        });

        foreach ($this->rarities as $rarity) {
            $x = new Rarity();
            $x->name = $rarity['name'];
            $x->save();
        }
    }
}
