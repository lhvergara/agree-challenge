<?php

namespace Database\Seeders;

use App\Repositories\Power\Power;
use App\Repositories\Power\PowerType;
use App\Repositories\Type\Type;
use Illuminate\Database\Seeder;

class PowersTableSeeder extends Seeder
{
    private $powers;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->powers = collect(json_decode(file_get_contents('database/json/powers.json'),true))->map(function($power) {
            return[
                'name' => $power['name'],
                'description' => $power['description'],
                'type_id' => $power['type_id'],
                'power_type_id' => $power['power_type_id'],
            ];
        });

        foreach ($this->powers as $power) {
            $x = new Power();
            $x->name = $power['name'];
            $x->description = $power['description'];
            $x->type_id = ($power['type_id']) ? Type::where('name',$power['type_id'])->first()->id : Type::where('name','Normal')->first()->id;
            $x->power_type_id = PowerType::where('name',$power['power_type_id'])->first()->id;
            $x->save();
        }
    }
}
