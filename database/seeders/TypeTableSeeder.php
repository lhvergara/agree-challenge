<?php

namespace Database\Seeders;

use App\Repositories\Type\Type;
use Illuminate\Database\Seeder;

class TypeTableSeeder extends Seeder
{
    private $types;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->types = collect(json_decode(file_get_contents('database/json/types.json'),true))->map(function($type) {
            return [
                'name' => $type['name'],
            ];
        });

        foreach ($this->types as $type) {
            $x = new Type();
            $x->name = $type['name'];
            $x->save();
        }
    }
}
