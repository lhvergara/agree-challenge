<?php

namespace Database\Seeders;

use App\Repositories\Card\Card;
use App\Repositories\Card\CardPower;
use App\Repositories\Expansion\Expansion;
use App\Repositories\Power\Power;
use App\Repositories\Rarity\Rarity;
use App\Repositories\Type\Type;
use Illuminate\Database\Seeder;

class CardTableSeeder extends Seeder
{
    private $cards;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->cards = collect(json_decode(file_get_contents('database/json/cards.json'),true))->map(function($card) {
            return [
                'name' => $card['name'],
                'type_id' => $card['type_id'],
                'weakness_id' => $card['weakness_id'],
            ];
        });

        foreach ($this->cards as $card) {
            $x = new Card();
            $x->name = $card['name'];
            $x->length = random_int(10,300);
            $x->weight = random_int(10,300);
            $x->hp = random_int(1,20) * 10;
            $x->price = random_int(100, 1000);
            $x->image = env('AWS_URL_DIRECTORY').'/charizard.jpg';
            $x->first_edition = random_int(0,1);
            $x->expansion_id = random_int(Expansion::min('id'),Expansion::max('id'));
            $x->type_id = Type::where('name',$card['type_id'])->first()->id;
            $x->weakness_id = ($card['weakness_id']) ? Type::where('name',$card['weakness_id'])->first()->id : null;
            $x->resistance_id = null;
            $x->resistance = random_int(0,10) * 10 * -1;
            $x->retreat_cost = random_int(0,3);
            $x->stage = random_int(1,3);
            $x->rarity_id = random_int(Rarity::min('id'),Rarity::max('id'));
            $x->save();

            $y = Power::select('powers.id as id')->join('types','powers.type_id','types.id')->where('types.name','Normal')->get();

            foreach ($y as $power) {
                $z = new CardPower();
                $z->card_id = $x->id;
                $z->power_id = $power['id'];
                $z->value = random_int(10,100);
                $z->cost = random_int(1,4);
                $z->save();
            }
        }
    }
}
