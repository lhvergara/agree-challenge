<?php

namespace Database\Seeders;

use App\Repositories\User\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $x = new User();
        $x->username = 'leandro';
        $x->password = Hash::make('12345678');
        $x->save();
    }
}
