<?php

namespace Database\Seeders;

use App\Repositories\Power\PowerType;
use Illuminate\Database\Seeder;

class PowerTypesTableSeeder extends Seeder
{
    private $powerTypes;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->powerTypes = collect(json_decode(file_get_contents('database/json/powers_type.json'),true))->map(function($powerType) {
            return [
                'name' => $powerType['name'],
            ];
        });

        foreach ($this->powerTypes as $powerType) {
            $x = new PowerType();
            $x->name = $powerType['name'];
            $x->save();
        }
    }
}
