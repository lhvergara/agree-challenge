<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('name',255)->unique();
            $table->unsignedSmallInteger('hp');
            $table->decimal('length',10,2,true);
            $table->decimal('weight',10,2,true);
            $table->decimal('price',10,2,true);
            $table->string('image',255)->nullable();
            $table->boolean('first_edition')->default(0);
            $table->TinyInteger('resistance')->default(0);
            $table->unsignedTinyInteger('retreat_cost')->default(0);
            $table->unsignedTinyInteger('stage')->default(0);

            $table->unsignedBigInteger('expansion_id');
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('weakness_id')->nullable();
            $table->unsignedBigInteger('resistance_id')->nullable();
            $table->unsignedBigInteger('rarity_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
