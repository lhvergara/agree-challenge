<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cards', function (Blueprint $table) {
            $table->foreign('expansion_id')->references('id')->on('expansions');
            $table->foreign('type_id')->references('id')->on('types');
            $table->foreign('weakness_id')->references('id')->on('types');
            $table->foreign('resistance_id')->references('id')->on('types');
            $table->foreign('rarity_id')->references('id')->on('rarities');
        });

        Schema::table('powers', function (Blueprint $table) {
            $table->foreign('type_id')->references('id')->on('types');
            $table->foreign('power_type_id')->references('id')->on('power_types');
        });

        Schema::table('card_powers', function (Blueprint $table) {
            $table->foreign('card_id')->references('id')->on('cards')->onDelete('cascade');
            $table->foreign('power_id')->references('id')->on('powers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cards', function (Blueprint $table) {
            $table->dropForeign(['expansion_id']);
            $table->dropForeign(['type_id']);
            $table->dropForeign(['weakness_id']);
            $table->dropForeign(['resistance_id']);
            $table->dropForeign(['rarity_id']);
        });

        Schema::table('powers', function (Blueprint $table) {
            $table->dropForeign(['type_id']);
            $table->dropForeign(['power_type_id']);
        });

        Schema::table('card_powers', function (Blueprint $table) {
            $table->dropForeign(['card_id']);
            $table->dropForeign(['power_id']);
        });
    }
}
