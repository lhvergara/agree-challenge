<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api', 'as' => 'api::'], function() use ($router) {
    $router->group(['prefix' => 'v1', 'as' => 'v1::'], function() use ($router) {
        $router->group(['prefix' => 'card', 'as' => 'card::', 'middleware' => 'auth'], function() use ($router) {
            $router->get('/{card_id}/get', 'Api\CardController@get');
            $router->get('/all[/{paginate}]', 'Api\CardController@getAll');
            $router->post('/report[/{paginate}]', 'Api\CardController@getByFilter');
            $router->post('/{card_id}/delete', 'Api\CardController@delete');
            $router->post('/{card_id}/update', 'Api\CardController@update');
            $router->post('/store', 'Api\CardController@store');
        });
        $router->group(['prefix' => 'auth', 'as' => 'auth::'], function() use ($router) {
            $router->post('/login', 'Auth\AuthController@login');
            $router->post('/register', 'Auth\AuthController@register');
            $router->group(['middleware' => 'auth'], function() use ($router) {
                $router->post('/logout', 'Auth\AuthController@logout');
                $router->post('/refresh', 'Auth\AuthController@refresh');
                $router->post('/me', 'Auth\AuthController@me');
            });
        });
    });
});
