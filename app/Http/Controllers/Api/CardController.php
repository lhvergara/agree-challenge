<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CardGetAllResource;
use App\Http\Resources\CardGetResource;
use App\Repositories\Card\Card;
use App\Repositories\Card\CardRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Http\Resources\CardGetAllPaginateResource;

class CardController extends Controller
{
    protected $card;
    protected $model;

    public function __construct(Card $model, CardRepository $card)
    {
        $this->model = $model;
        $this->card = $card;
    }

    /**
    * @OA\Get(
    *     path="/api/v1/card/get/{card_id}",
    *     operationId="/api/v1/card/get/{card_id}",
    *     tags={"card"},
    *     @OA\Parameter(
    *         name="card_id",
    *         in="path",
    *         description="The card id",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Response(
    *         response="200",
    *         description="Card Information",
    *         @OA\JsonContent()
    *     ),
    *     @OA\Response(
    *         response="400",
    *         description="Error: Bad request. When required parameters were supplied incorrect.",
    *     ),
    *     @OA\Response(
    *         response="401",
    *         description="Error: Unauthorized",
    *     ),
    * )
    */
    public function get(Request $request) {
        $data = ['card_id' => $request->card_id ];
        $validator = Validator::make($data, [
            'card_id' => 'required|numeric|exists:cards,id'
        ]);

        if ($validator->fails()) {
            return $this->apiReponse([],$validator->messages(),Response::HTTP_BAD_REQUEST);
        }

        return $this->apiReponse(new CardGetResource($this->card->find($request->card_id)));
    }

    /**
    * @OA\Get(
    *     path="/api/v1/card/all/{paginate?}",
    *     operationId="/api/v1/card/all/{paginate?}",
    *     tags={"card"},
    *     @OA\Parameter(
    *         name="paginate",
    *         in="path",
    *         description="Paginate 10 25 50 100",
    *         required=false,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Response(
    *         response="200",
    *         description="Get all card success",
    *         @OA\JsonContent()
    *     ),
    *     @OA\Response(
    *         response="400",
    *         description="Error: Bad request. When required parameters were supplied incorrect.",
    *     ),
    *     @OA\Response(
    *         response="401",
    *         description="Error: Unauthorized",
    *     ),
    * )
    */

    public function getAll(Request $request, $paginate = null) {
        $data = ['paginate' => $paginate];
        $validator = Validator::make($data, [
            'paginate' => [
                'nullable',
                'numeric',
                Rule::in([10, 25, 50, 100])
            ]
        ]);

        if ($validator->fails()) {
            return $this->apiReponse([],$validator->messages(),Response::HTTP_BAD_REQUEST);
        }

        return ($data['paginate']) ? $this->apiReponse($this->card->all(['*'],$data['paginate'])) : $this->apiReponse(new CardGetAllResource($this->card->all(['*'])));
    }

    /**
    * @OA\Post(
    *     path="/api/v1/card/report/{paginate?}",
    *     operationId="/api/v1/card/all/{paginate?}",
    *     tags={"card"},
    *     @OA\Parameter(
    *         name="paginate",
    *         in="path",
    *         description="Paginate 10 25 50 100",
    *         required=false,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="first_edition",
    *         in="path",
    *         description="Card First Edition Check Filter",
    *         required=false,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="stage",
    *         in="path",
    *         description="Pokemon stage id",
    *         required=false,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="expansion_id",
    *         in="path",
    *         description="Expansion Card Set Id Filter",
    *         required=false,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="type_id",
    *         in="path",
    *         description="Card Type Id Filter",
    *         required=false,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="weakness_id",
    *         in="path",
    *         description="Weakness Card Id Filter",
    *         required=false,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="resistance_id",
    *         in="path",
    *         description="Card Resistance Id Filter",
    *         required=false,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="rarity_id",
    *         in="path",
    *         description="Card Rarity Id Filter",
    *         required=false,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Response(
    *         response="200",
    *         description="Get all card success",
    *         @OA\JsonContent()
    *     ),
    *     @OA\Response(
    *         response="400",
    *         description="Error: Bad request. When required parameters were supplied incorrect.",
    *     ),
    *     @OA\Response(
    *         response="401",
    *         description="Error: Unauthorized",
    *     ),
    * )
    */

    public function getByFilter(Request $request, $paginate = null) {
        $data = [
            'paginate' => $paginate,
            'first_edition' => $request->first_edition,
            'stage' => $request->stage,
            'expansion_id' => $request->expansion_id,
            'type_id' => $request->type_id,
            'weakness_id' => $request->weakness_id,
            'resistance_id' => $request->resistance_id,
            'rarity_id' => $request->rarity_id
        ];

        $validator = Validator::make($data, [
            'paginate' => [
                'nullable',
                'numeric',
                Rule::in([10, 25, 50, 100])
            ],
            'first_edition' => 'nullable|boolean',
            'stage' => 'nullable|numeric',
            'expansion_id' => 'nullable|numeric|exists:expansions,id',
            'type_id' => 'nullable|numeric|exists:types,id',
            'weakness_id' => 'nullable|numeric|exists:types,id',
            'resistance_id' => 'nullable|numeric|exists:types,id',
            'rarity_id' => 'nullable|numeric|exists:rarities,id',
        ]);

        if ($validator->fails()) {
            return $this->apiReponse([],$validator->messages(),Response::HTTP_BAD_REQUEST);
        }

        return ($data['paginate']) ? $this->apiReponse($this->card->getByFilter($data,$paginate)) : $this->apiReponse(new CardGetAllResource($this->card->getByFilter($data)));
    }

    /**
    * @OA\Post(
    *     path="/api/v1/card/{card_id}/delete",
    *     operationId="/api/v1/card/{card_id}/delete",
    *     tags={"card"},
    *     @OA\Parameter(
    *         name="card_id",
    *         in="path",
    *         description="The card id to Delete",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Response(
    *         response="200",
    *         description="Card deleted",
    *         @OA\JsonContent()
    *     ),
    *     @OA\Response(
    *         response="400",
    *         description="Error: Bad request. When required parameters were supplied incorrect.",
    *     ),
    *     @OA\Response(
    *         response="401",
    *         description="Error: Unauthorized",
    *     ),
    * )
    */

    public function delete($card_id) {
        $data = ['card_id' => $card_id];
        $validator = Validator::make($data, [
            'card_id' => 'required|numeric|exists:cards,id'
        ]);

        if ($validator->fails()) {
            return $this->apiReponse([],$validator->messages(),Response::HTTP_BAD_REQUEST);
        }

        return $this->apiReponse(new CardGetResource($this->card->delete($card_id)));
    }

    /**
    * @OA\Post(
    *     path="/api/v1/card/{card_id}/update",
    *     operationId="/api/v1/card/{card_id}/update",
    *     tags={"card"},
    *     @OA\Parameter(
    *         name="id",
    *         in="path",
    *         description="id",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="name",
    *         in="path",
    *         description="name",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="length",
    *         in="path",
    *         description="length",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="weight",
    *         in="path",
    *         description="weight",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="hp",
    *         in="path",
    *         description="hp",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="price",
    *         in="path",
    *         description="price",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="image",
    *         in="path",
    *         description="image",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="first_edition",
    *         in="path",
    *         description="first_edition",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="expansion_id",
    *         in="path",
    *         description="expansion_id",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="type_id",
    *         in="path",
    *         description="type_id",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="weakness_id",
    *         in="path",
    *         description="weakness_id",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="resistance_id",
    *         in="path",
    *         description="resistance_id",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="resistance",
    *         in="path",
    *         description="resistance",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="rarity_id",
    *         in="path",
    *         description="rarity_id",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Response(
    *         response="200",
    *         description="Card Updated",
    *         @OA\JsonContent()
    *     ),
    *     @OA\Response(
    *         response="400",
    *         description="Error: Bad request. When required parameters were supplied incorrect.",
    *     ),
    *     @OA\Response(
    *         response="401",
    *         description="Error: Unauthorized",
    *     ),
    * )
    */

    public function update(Request $request, $card_id) {
        $data = [
            'id' => $card_id,
            'name' => $request->name,
            'length' => $request->length,
            'weight' => $request->weight,
            'hp' => $request->hp,
            'price' => $request->price,
            'image' => $request->file('image'),
            'first_edition' => $request->first_edition,
            'expansion_id' => $request->expansion_id,
            'type_id' => $request->type_id,
            'weakness_id' => ($request->weakness_id) ? $request->weakness_id : null,
            'resistance_id' => ($request->resistance_id) ? $request->resistance_id : null,
            'resistance' => $request->resistance,
            'rarity_id' => ($request->rarity_id) ? $request->rarity_id : null
        ];

        $validator = Validator::make($data, [
            'id' => 'required|numeric|exists:cards,id',
            'expansion_id' => 'nullable|numeric|exists:expansions,id',
            'type_id' => 'nullable|numeric|exists:types,id',
            'weakness_id' => 'nullable|numeric|exists:types,id',
            'resistance_id' => 'nullable|numeric|exists:types,id',
            'rarity_id' => 'nullable|numeric|exists:rarities,id',
            'name' => ['nullable','max:255','string',Rule::unique('cards','name')->ignore($data['id'],'id')],
            'length' => 'nullable|numeric|min:10|max:300',
            'weight' => 'nullable|numeric|min:10|max:300',
            'hp' => 'nullable|numeric',
            'price' => 'nullable|numeric',
            'image' => 'nullable|image',
            'first_edition' => 'nullable|boolean',
        ]);

        if ($validator->fails()) {
            return $this->apiReponse([],$validator->messages(),Response::HTTP_BAD_REQUEST);
        }

        return $this->apiReponse(new CardGetResource($this->card->update($this->model,array_filter($data), $request->file('image'))));
    }

    /**
    * @OA\Post(
    *     path="/api/v1/card/store",
    *     operationId="/api/v1/card/store",
    *     tags={"card"},
    *     @OA\Parameter(
    *         name="name",
    *         in="path",
    *         description="name",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="length",
    *         in="path",
    *         description="length",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="weight",
    *         in="path",
    *         description="weight",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="hp",
    *         in="path",
    *         description="hp",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="price",
    *         in="path",
    *         description="price",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="image",
    *         in="path",
    *         description="image",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="first_edition",
    *         in="path",
    *         description="first_edition",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="expansion_id",
    *         in="path",
    *         description="expansion_id",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="type_id",
    *         in="path",
    *         description="type_id",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="weakness_id",
    *         in="path",
    *         description="weakness_id",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="resistance_id",
    *         in="path",
    *         description="resistance_id",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="resistance",
    *         in="path",
    *         description="resistance",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="rarity_id",
    *         in="path",
    *         description="rarity_id",
    *         required=true,
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Response(
    *         response="200",
    *         description="Card Stored",
    *         @OA\JsonContent()
    *     ),
    *     @OA\Response(
    *         response="400",
    *         description="Error: Bad request. When required parameters were supplied incorrect.",
    *     ),
    *     @OA\Response(
    *         response="401",
    *         description="Error: Unauthorized",
    *     ),
    * )
    */

    public function store(Request $request) {
        $data = [
            'name' => $request->name,
            'length' => $request->length,
            'weight' => $request->weight,
            'hp' => $request->hp,
            'price' => $request->price,
            'image' => null,
            'first_edition' => $request->first_edition,
            'expansion_id' => $request->expansion_id,
            'type_id' => $request->type_id,
            'weakness_id' => ($request->weakness_id) ? $request->weakness_id : null,
            'resistance_id' => ($request->resistance_id) ? $request->resistance_id : null,
            'resistance' => $request->resistance,
            'rarity_id' => ($request->rarity_id) ? $request->rarity_id : null
        ];

        $validator = Validator::make($data, [
            'expansion_id' => 'required|numeric|exists:expansions,id',
            'type_id' => 'required|numeric|exists:types,id',
            'weakness_id' => 'nullable|numeric|exists:types,id',
            'resistance_id' => 'nullable|numeric|exists:types,id',
            'rarity_id' => 'required|numeric|exists:rarities,id',
            'name' => ['required','max:255','string',Rule::unique('cards','name')],
            'length' => 'required|numeric|min:10|max:300',
            'weight' => 'required|numeric|min:10|max:300',
            'hp' => 'required|numeric',
            'price' => 'required|numeric',
            'image' => 'nullable|image',
            'first_edition' => 'required|boolean',
        ]);

        if ($validator->fails()) {
            return $this->apiReponse([],$validator->messages(),Response::HTTP_BAD_REQUEST);
        }

        return $this->apiReponse(new CardGetResource($this->card->create($this->model,$data,$request->file('image'))));
    }
}
