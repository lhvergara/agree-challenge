<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *   title="Pokemon Challenge API - Agree Backend Challenge",
     *  version="1.0.0",
     *  @OA\Contact(
     *    email="vergara.leandro@gmail.com",
     *    name="Leandro Hugo Vergara"
     *  )
     * )
     */

    protected function apiReponse($data = [], $message = '', $http_code = Response::HTTP_OK, $token = null)
    {
        $status = null;

        switch ($http_code) {
            case Response::HTTP_OK:
                $status = 'success';
                break;
            default:
                $status = 'error';
                break;
        }

        $response = [
            'date' => Carbon::now()->toDateTimeString(),
            'status' => $status,
            'data' => $data,
        ];

        if ($message) {
            $response['message'] = $message;
        }

        if ($token) {
            $response['token'] = $token;
        }

        return response()->json($response, $http_code);
    }
}
