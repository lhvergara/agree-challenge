<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserStoreResource;
use App\Repositories\User\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;

class AuthController extends Controller
{
    protected $model;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(User $model)
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
        $this->model = $model;
    }

    /**
    * @OA\Post(
    *     path="/api/v1/auth/register",
    *     operationId="/api/v1/auth/register",
    *     tags={"auth"},
    *     @OA\Parameter(
    *         name="username",
    *         in="path",
    *         description="Username",
    *         required=true,
    *         @OA\Schema(type="string")
    *     ),
    *     @OA\Parameter(
    *         name="password",
    *         in="path",
    *         description="Password",
    *         required=true,
    *         @OA\Schema(type="string")
    *     ),
    *     @OA\Response(
    *         response="200",
    *         description="Register OK!",
    *         @OA\JsonContent()
    *     ),
    *     @OA\Response(
    *         response="400",
    *         description="Error: Bad request. When required parameters were supplied incorrect.",
    *     ),
    * )
    */

    public function register(Request $request) {
        $data = [
            'username' => $request->username,
            'password' => $request->password,
            'password_confirmation' => $request->password_confirmation,
        ];

        $validator = Validator::make($data, [
            'username' => 'required|unique:users,username',
            'password' => 'required|confirmed'
        ]);

        if ($validator->fails()) {
            return $this->apiReponse([],$validator->messages(),Response::HTTP_BAD_REQUEST);
        }

        $user = User::create(['username' => $request->username, 'password' => Hash::make($request->password)]);

        return $this->apiReponse(new UserStoreResource($user),'Register OK!');
    }

    /**
    * @OA\Post(
    *     path="/api/v1/auth/login",
    *     operationId="/api/v1/auth/login",
    *     tags={"auth"},
    *     @OA\Parameter(
    *         name="username",
    *         in="path",
    *         description="Username",
    *         required=true,
    *         @OA\Schema(type="string")
    *     ),
    *     @OA\Parameter(
    *         name="password",
    *         in="path",
    *         description="Password",
    *         required=true,
    *         @OA\Schema(type="string")
    *     ),
    *     @OA\Response(
    *         response="200",
    *         description="Login OK!",
    *         @OA\JsonContent()
    *     ),
    *     @OA\Response(
    *         response="400",
    *         description="Error: Bad request. When required parameters were supplied incorrect.",
    *     ),
    * )
    */

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $data = [
            'username' => $request->username,
            'password' => $request->password,
        ];

        $validator = Validator::make($data, [
            'username' => 'required|string',
            'password' => 'required|string'
        ]);

        if ($validator->fails()) {
            return $this->apiReponse([],$validator->messages(),Response::HTTP_BAD_REQUEST);
        }

        $credentials = $request->only(['username', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return $this->apiReponse([],'Unauthorized!',Response::HTTP_UNAUTHORIZED);
        }

        return $this->apiReponse(new UserStoreResource($this->model->where('username',$data['username'])->first()),'Login OK!',Response::HTTP_OK,$this->respondWithToken($token));
    }

    /**
    * @OA\Post(
    *     path="/api/v1/auth/me",
    *     operationId="/api/v1/auth/me",
    *     tags={"auth"},
    *     @OA\Response(
    *         response="200",
    *         description="Logged User Test Debug OK!",
    *         @OA\JsonContent()
    *     ),
    *     @OA\Response(
    *         response="401",
    *         description="Error: Unauthorized",
    *     ),
    * )
    */

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return $this->apiReponse(new UserStoreResource(auth()->user()),'User JWT Test',Response::HTTP_OK);
    }

    /**
    * @OA\Post(
    *     path="/api/v1/auth/logout",
    *     operationId="/api/v1/auth/logout",
    *     tags={"auth"},
    *     @OA\Response(
    *         response="200",
    *         description="User Logged out OK!",
    *         @OA\JsonContent()
    *     ),
    *     @OA\Response(
    *         response="401",
    *         description="Error: Unauthorized",
    *     ),
    * )
    */

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return $this->apiReponse([],'Logout!');
    }

    /**
    * @OA\Post(
    *     path="/api/v1/auth/refresh",
    *     operationId="/api/v1/auth/refresh",
    *     tags={"auth"},
    *     @OA\Response(
    *         response="200",
    *         description="Refresh JWT Token OK!",
    *         @OA\JsonContent()
    *     ),
    *     @OA\Response(
    *         response="401",
    *         description="Error: Unauthorized",
    *     ),
    * )
    */

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->apiReponse(new UserStoreResource(auth()->user()),'Refresh Token!',Response::HTTP_OK,$this->respondWithToken(auth()->refresh(true,true)));
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * env('SESSION_TTL')
        ];
    }
}
