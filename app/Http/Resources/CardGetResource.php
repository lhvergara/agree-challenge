<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CardGetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'length' => $this->resource->length,
            'weight' => $this->resource->weight,
            'hp' => $this->resource->hp,
            'price' => $this->resource->price,
            'image' => ($this->resource->image) ? env('AWS_URL_UPLOADS').$this->resource->image : null,
            'first_edition' => $this->resource->first_edition,
            'retreat_cost' => $this->resource->retreat_cost,
            'stage' => $this->resource->stage,
            'expansion_id' => $this->resource->expansion->name,
            'type_id' => $this->resource->type->name,
            'weakness_id' => ($this->resource->weakness) ? $this->resource->weakness->name : null,
            'resistance_id' => ($this->resource->resistance_type) ? $this->resource->resistance_type->name : null,
            'resistance' => $this->resource->resistance,
            'rarity_id' => $this->resource->rarity->name,
            'powers' =>  $this->powers->map(function($power) {
                return [
                    'name' => $power->name,
                    'description' => $power->description,
                    'type' => $power->type->name,
                    'power_type' => $power->power_type->name
                ];
            })
        ];
    }
}
