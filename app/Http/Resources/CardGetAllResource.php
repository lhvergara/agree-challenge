<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CardGetAllResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource->map(function ($card) {
            return [
                'id' => $card->id,
                'name' => $card->name,
                'length' => $card->length,
                'weight' => $card->weight,
                'hp' => $card->hp,
                'price' => $card->price,
                'image' => ($card->image) ? env('AWS_URL_UPLOADS').$card->image : null,
                'first_edition' => $card->first_edition,
                'retreat_cost' => $card->retreat_cost,
                'stage' => $card->stage,
                'expansion_id' => ($card->expansion) ? $card->expansion->name : null,
                'type_id' => ($card->type) ? $card->type->name : null,
                'weakness_id' => ($card->weakness) ? $card->weakness->name : null,
                'resistance_id' => ($card->resistance_type) ? $card->resistance_type->name : null,
                'resistance' => $card->resistance,
                'rarity_id' => ($card->rarity) ? $card->rarity->name : null,
                'powers' =>  $card->powers->map(function($power) {
                    return [
                        'name' => $power->name,
                        'description' => $power->description,
                        'type' => $power->type->name,
                        'power_type' => $power->power_type->name
                    ];
                })
            ];
        });
    }
}
