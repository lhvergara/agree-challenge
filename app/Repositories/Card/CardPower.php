<?php

namespace App\Repositories\Card;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CardPower
 * @property integer $id
 * @property integer $card_id
 * @property integer $power_id
 * @property integer $value
 * @property integer $cost
 *
 * @package App\Repositories\Card\CardPower
 * @OA\Schema(
 *     schema="CardPower",
 *     type="object",
 *     title="CardPower",
 *     required={"id","card_id","power_id","value","cost"},
 *     properties={
 *         @OA\Property(property="id", type="integer"),
 *         @OA\Property(property="card_id", type="integer"),
 *         @OA\Property(property="power_id", type="integer"),
 *         @OA\Property(property="value", type="integer"),
 *         @OA\Property(property="cost", type="integer"),
 *     }
 * )
 */

class CardPower extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'card_id', 'power_id', 'value', 'cost'
    ];
}
