<?php

namespace App\Repositories\Card;

use App\Repositories\Expansion\Expansion;
use App\Repositories\Power\Power;
use App\Repositories\Rarity\Rarity;
use App\Repositories\Type\Type;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Card
 * @property integer $id
 * @property string  $name
 * @property integer $length
 * @property integer $weight
 * @property integer $hp
 * @property decimal $price
 * @property string  $image
 * @property integer $first_edition
 * @property integer $expansion_id
 * @property integer $type_id
 * @property integer $weakness_id
 * @property integer $resistance_id
 * @property integer $resistance
 * @property integer $rarity_id
 *
 * @package App\Repositories\Card\Card
 * @OA\Schema(
 *     schema="Card",
 *     type="object",
 *     title="Card",
 *     required={"id","name","length","weight","hp","price","image","stage","first_edition","expansion_id","type_id","weakness_id","resistance_id","resistance","rarity_id"},
 *     properties={
 *         @OA\Property(property="id", type="integer"),
 *         @OA\Property(property="name", type="string"),
 *         @OA\Property(property="length", type="integer"),
 *         @OA\Property(property="weight", type="integer"),
 *         @OA\Property(property="hp", type="integer"),
 *         @OA\Property(property="price", type="integer"),
 *         @OA\Property(property="image", type="string"),
 *         @OA\Property(property="stage", type="integer"),
 *         @OA\Property(property="first_edition", type="integer"),
 *         @OA\Property(property="expansion_id", type="integer"),
 *         @OA\Property(property="type_id", type="integer"),
 *         @OA\Property(property="weakness_id", type="integer"),
 *         @OA\Property(property="resistance_id", type="integer"),
 *         @OA\Property(property="resistance", type="integer"),
 *         @OA\Property(property="rarity_id", type="integer")
 *     }
 * )
 */
class Card extends Model
{
    protected $table = 'cards';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'length',
        'weight',
        'hp',
        'price',
        'image',
        'retreat_cost',
        'stage',
        'first_edition',
        'expansion_id',
        'type_id',
        'weakness_id',
        'resistance_id',
        'resistance',
        'rarity_id'
    ];

    // Eloquent Relantionships
    public function expansion() {
        return $this->hasOne(Expansion::class,'id','expansion_id');
    }

    public function type() {
        return $this->hasOne(Type::class,'id','type_id');
    }

    public function weakness() {
        return $this->hasOne(Type::class,'id','weakness_id');
    }

    public function resistance_type() {
        return $this->hasOne(Type::class,'id','resistance_id');
    }

    public function rarity() {
        return $this->hasOne(Rarity::class,'id','rarity_id');
    }

    public function powers() {
        return $this->belongsToMany(Power::class,'card_powers');
    }
}
