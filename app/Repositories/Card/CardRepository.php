<?php

namespace App\Repositories\Card;

use App\Repositories\Base\BaseRepository;

class CardRepository extends BaseRepository
{
    protected $model;

    public function __construct(Card $card)
    {
        $this->model = $card;
    }

    public function getByFilter($data,$paginate = null) {
        $query = Card::with('expansion','type','weakness','resistance_type','rarity','powers');

        if (isset($data['first_edition'])) {
            $query->where('first_edition',$data['first_edition']);
        }

        if (isset($data['stage'])) {
            $query->where('stage',$data['stage']);
        }

        if ($data['expansion_id']) {
            $query->where('expansion_id', $data['expansion_id']);
        }

        if ($data['type_id']) {
            $query->where('type_id',$data['type_id']);
        }

        if ($data['weakness_id']) {
            if($data['weakness_id'] == 0) {
                $query->whereNull('weakness_id');
            } else {
                $query->where('weakness_id',$data['weakness_id']);
            }
        }

        if ($data['resistance_id']) {
            if($data['resistance_id'] == 0) {
                $query->whereNull('resistance_id');
            } else {
                $query->where('resistance_id',$data['resistance_id']);
            }
        }

        if ($data['rarity_id']) {
            $query->where('rarity_id',$data['rarity_id']);
        }

        return ($paginate) ? $query->paginate($paginate) : $query->orderBy('name')->get();
    }
}
