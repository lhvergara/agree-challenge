<?php

namespace App\Repositories\Power;

use App\Repositories\Type\Type;
use Illuminate\Database\Eloquent\Model;

/**
* Class Power
* @property integer $id
* @property string  $name
* @property string  $description
* @property integer  $type_id
* @property integer  $power_type_id
*
* @package App\Repositories\Power\Power
* @OA\Schema(
*     schema="Power",
*     type="object",
*     title="Power",
*     required={"id","name","description","type_id","power_type_id"},
*     properties={
*         @OA\Property(property="id", type="integer"),
*         @OA\Property(property="name", type="string"),
*         @OA\Property(property="description", type="string"),
*         @OA\Property(property="type_id", type="integer"),
*         @OA\Property(property="power_type_id", type="integer")
*     }
* )
*/

class Power extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'description', 'type_id', 'power_type_id'
    ];

    public function type() {
        return $this->hasOne(Type::class,'id','type_id');
    }

    public function power_type() {
        return $this->hasOne(PowerType::class,'id','power_type_id');
    }
}
