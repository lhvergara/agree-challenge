<?php

namespace App\Repositories\Power;

use Illuminate\Database\Eloquent\Model;

/**
* Class PowerType
* @property integer $id
* @property string  $name
*
* @package App\Repositories\Power\PowerType
* @OA\Schema(
*     schema="PowerType",
*     type="object",
*     title="PowerType",
*     required={"id","name"},
*     properties={
*         @OA\Property(property="id", type="integer"),
*         @OA\Property(property="name", type="string"),
*     }
* )
*/

class PowerType extends Model
{
    protected $table = "power_types";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name'
    ];
}
