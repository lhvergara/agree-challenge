<?php

namespace App\Repositories\Base;

use Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{
    public function all();

    public function find($id);

    public function findOrFail($id);

    public function create($model, array $data);

    public function update($model, array $data);

    public function delete($model);

    public function uploadFile($file, $directory);
}
