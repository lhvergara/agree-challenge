<?php

namespace App\Repositories\Base;

use Exception;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Filesystem\Filesystem;
use League\Flysystem\AwsS3v3\AwsS3Adapter;

abstract class BaseRepository implements RepositoryInterface
{
    /**
     * The Model instance.
     *
     * @var Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * Get model collection
     *
     * @param array $attributes
     *
     * @return collection
     */
    public function all($attributes = ['*'], $paginate = null)
    {
        return ($paginate) ? $this->model->paginate($paginate) : $this->model->all($attributes);
    }

    /**
     * Find a model
     *
     * @param int $id
     * @return object
     */
    public function find($id)
    {
        return $this->model->find($id);
    }

    /**
     * Find a model, if the model doesn't exist throws an exception ModelNotFoundException
     *
     * @param int $id
     * @return object
     */
    public function findOrFail($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Create a model.
     *
     * @param array $data
     * @return object
     */
    public function create($model, array $data, $file = null)
    {
        if($file) {
            $data['image'] = $this->uploadFile($file,env('AWS_URL_DIRECTORY'));
        }

        return $model->create($data);
    }

    /**
     * Update a model.
     *
     * @param object $model
     * @param array $data
     * @return object
     */
    public function update($model, array $data, $file = null)
    {
        if($file) {
            $data['image'] = $this->uploadFile($file,env('AWS_URL_DIRECTORY'));
        }

        $model = $model->find($data['id']);
        $model->fill($data);

        $model->save();

        return $model;
    }

    /**
     * Delete a model.
     *
     * @param object $model
     * @return object
     */
    public function delete($model)
    {
        if (is_numeric($model)) {
            $model = $this->findOrFail($model);
        }

        if($model->image) {
            $this->deleteFile($model->image);
        }

        $model->delete();

        return $model;
    }

    /**
     * Fill a model.
     *
     * @param $data
     * @return mixed
     */
    public function fresh($data)
    {
        return $this->model->fill($data);
    }

    /**
     * Get a paginator for the "select" statement.
     *
     * @param $limit
     * @return mixed
     */
    public function paginate($limit)
    {
        return $this->model->paginate($limit);
    }

    /**
     * Get the fillable attributes for the model.
     *
     * @return mixed
     */
    public function getFillable()
    {
        return $this->model->getFillable();
    }

    /**
     * @param $file
     * @param $directory
     * @return string
     * @throws Exception
     */
    public function uploadFile($file, $directory)
    {
        $url = Storage::disk('s3')->put($directory, $file);
        if (!$url) {
            throw new Exception('Error uploading file!');
        }
        return $url;
    }

    /**
     * @param $file
     * @param $directory
     * @return string
     * @throws Exception
     */
    public function deleteFile($file)
    {
        return (Storage::disk('s3')->exists($file)) ? Storage::disk('s3')->delete($file) : false;
    }
}
