<?php

namespace App\Repositories\Type;

use Illuminate\Database\Eloquent\Model;

/**
* Class Type
* @property integer $id
* @property string  $name
*
* @package App\Repositories\Type\Type
* @OA\Schema(
*     schema="Type",
*     type="object",
*     title="Type",
*     required={"id","name"},
*     properties={
*         @OA\Property(property="id", type="integer"),
*         @OA\Property(property="name", type="string"),
*     }
* )
*/

class Type extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name'
    ];
}
