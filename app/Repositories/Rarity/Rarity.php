<?php

namespace App\Repositories\Rarity;

use Illuminate\Database\Eloquent\Model;

/**
* Class Rarity
* @property integer $id
* @property string  $name
*
* @package App\Repositories\Rarity\Rarity
* @OA\Schema(
*     schema="Rarity",
*     type="object",
*     title="Rarity",
*     required={"id","name"},
*     properties={
*         @OA\Property(property="id", type="integer"),
*         @OA\Property(property="name", type="string"),
*     }
* )
*/

class Rarity extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name'
    ];
}
