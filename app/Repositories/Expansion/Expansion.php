<?php

namespace App\Repositories\Expansion;

use Illuminate\Database\Eloquent\Model;

/**
* Class Expansion
* @property integer $id
* @property string  $name
*
* @package App\Repositories\Expansion\Expansion
* @OA\Schema(
*     schema="Expansion",
*     type="object",
*     title="Expansion",
*     required={"id","name"},
*     properties={
*         @OA\Property(property="id", type="integer"),
*         @OA\Property(property="name", type="string")
*     }
* )
*/

class Expansion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name'
    ];
}
