<?php

use App\Repositories\Card\Card;
use App\Repositories\User\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Testing\WithoutMiddleware;

class PokemonTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function debug()
    {
        $this->assertTrue(true);
    }

    public function test_error_404_handler_exception() {
        $response = $this->get('/asd');

        $response->assertResponseStatus(Response::HTTP_NOT_FOUND);
    }

    public function test_error_400_handler_exception() {
        $response = $this->get('/api/v1/auth/login');

        $response->assertResponseStatus(Response::HTTP_METHOD_NOT_ALLOWED);
    }

    public function test_middleware_auth_unauthorized_routes() {
        $response = $this->get('/api/v1/card/1/get');
        $response->assertResponseStatus(Response::HTTP_UNAUTHORIZED);

        $response = $this->get('/api/v1/card/all');
        $response->assertResponseStatus(Response::HTTP_UNAUTHORIZED);

        $response = $this->post('/api/v1/card/report');
        $response->assertResponseStatus(Response::HTTP_UNAUTHORIZED);

        $response = $this->post('/api/v1/card/1/delete');
        $response->assertResponseStatus(Response::HTTP_UNAUTHORIZED);

        $response = $this->post('/api/v1/card/1/update');
        $response->assertResponseStatus(Response::HTTP_UNAUTHORIZED);

        $response = $this->post('/api/v1/card/store');
        $response->assertResponseStatus(Response::HTTP_UNAUTHORIZED);

        $response = $this->post('/api/v1/auth/logout');
        $response->assertResponseStatus(Response::HTTP_UNAUTHORIZED);

        $response = $this->post('/api/v1/auth/refresh');
        $response->assertResponseStatus(Response::HTTP_UNAUTHORIZED);

        $response = $this->post('/api/v1/auth/me');
        $response->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function test_user_login_success() {
        $credentials = ['username' => 'leandro', 'password' => '12345678'];

        $response = $this->post('/api/v1/auth/login',$credentials,['Content-Type' => 'application/x-www-form-urlencoded']);

        $response->assertResponseStatus(Response::HTTP_OK);

        $response->seeJson(["message" => "Login OK!"]);
    }

    public function test_user_login_error() {
        $credentials = ['username' => 'leandro', 'password' => '123456789'];

        $response = $this->post('/api/v1/auth/login',$credentials,['Content-Type' => 'application/x-www-form-urlencoded']);
        $response->assertResponseStatus(Response::HTTP_UNAUTHORIZED);

        $credentials = ['username' => '', 'password' => ''];

        $response = $this->post('/api/v1/auth/login',$credentials,['Content-Type' => 'application/x-www-form-urlencoded']);
        $response->assertResponseStatus(Response::HTTP_BAD_REQUEST);

        $credentials = ['username' => 'leandro', 'password' => ''];

        $response = $this->post('/api/v1/auth/login',$credentials,['Content-Type' => 'application/x-www-form-urlencoded']);
        $response->assertResponseStatus(Response::HTTP_BAD_REQUEST);
    }

    public function test_user_register_success() {
        $credentials = ['username' => 'roberto', 'password' => '12345678', 'password_confirmation' => '12345678'];

        $response = $this->post('/api/v1/auth/register',$credentials,['Content-Type' => 'application/x-www-form-urlencoded']);

        $response->assertResponseStatus(Response::HTTP_OK);

        $response->seeJson(["message" => "Register OK!"]);

        $u = User::where('username','roberto');
        $u->delete();
    }

    public function test_user_register_error_username_already_exists() {
        $credentials = ['username' => 'leandro', 'password' => '12345678', 'password_confirmation' => '12345678'];

        $response = $this->post('/api/v1/auth/register',$credentials,['Content-Type' => 'application/x-www-form-urlencoded']);

        $response->assertResponseStatus(Response::HTTP_BAD_REQUEST);

        $response->seeJson(["status" => "error"]);
    }

    public function test_error_400_api_routes() {
        $this->withoutMiddleware();

        $response = $this->get('/api/v1/card/a/get');
        $response->assertResponseStatus(Response::HTTP_BAD_REQUEST);

        $response = $this->get('/api/v1/card/9999/get');
        $response->assertResponseStatus(Response::HTTP_BAD_REQUEST);
    }

    public function test_error_200_api_routes() {
        $this->withoutMiddleware();

        $response = $this->get('/api/v1/card/1/get');
        $response->assertResponseStatus(Response::HTTP_OK);

        $response = $this->get('/api/v1/card/all');
        $response->assertResponseStatus(Response::HTTP_OK);

        $response = $this->get('/api/v1/card/all/25');
        $response->assertResponseStatus(Response::HTTP_OK);

        $response = $this->post('/api/v1/card/report');
        $response->assertResponseStatus(Response::HTTP_OK);

        $response = $this->post('/api/v1/card/report/25');
        $response->assertResponseStatus(Response::HTTP_OK);
    }

    public function test_create_update_card() {
        $this->withoutMiddleware();

        $data = [
            'id' => null,
            'name' => 'Card Test '.random_int(1,300),
            'length' => random_int(10,300),
            'weight' => random_int(10,300),
            'hp' => random_int(1,20) * 10,
            'price' => random_int(100, 1000),
            'image' => null,
            'first_edition' => random_int(0,1),
            'expansion_id' => 1,
            'type_id' => 1,
            'weakness_id' => 1,
            'resistance_id' => 1,
            'resistance' => random_int(0,10) * 10 * -1,
            'retreat_cost' => random_int(0,3),
            'stage' => random_int(1,3),
            'rarity_id' => 1,
        ];

        $response = $this->post('/api/v1/card/store',$data,['Content-Type' => 'application/x-www-form-urlencoded']);

        $response->assertResponseStatus(Response::HTTP_OK);

        $response->seeJson(["status" => "success"]);

        $card_id = DB::table('cards')->max('id');

        $data['id'] = $card_id;
        $data['name'] = 'Card Test '.random_int(1,300);

        $response = $this->post('/api/v1/card/'.$data['id'].'/update',$data,['Content-Type' => 'application/x-www-form-urlencoded']);

        $response->assertResponseStatus(Response::HTTP_OK);

        $response->seeJson(["status" => "success"]);
    }

    public function test_delete_card() {
        $this->withoutMiddleware();

        $card_id = DB::table('cards')->max('id');

        $response = $this->post('/api/v1/card/'.$card_id.'/delete');

        $response->assertResponseStatus(Response::HTTP_OK);

        $response->seeJson(["status" => "success"]);
    }
}
